# MOCKIST TDD Bank Kata

This is a kata to practice mockist TDD from an outside-in approach is available in Katalist Codurance. See [his blog post](https://katalyst.codurance.com/bank)

### Kata: Bank Kata rules
1- One level of indentation per method

2- Don’t use the ELSE keyword

3- Wrap all primitives and Strings

4- First class collections

6- One dot per line

7- Don’t abbreviate

8- Keep all entities small (50 lines)

9- No classes with more than two instance variables

10- No getters/setters/properties


### Kata: Bank Kata requirement
- Deposit and Withdrawal
- Account statement (date, amount, balance)
- Operation printing

### Tip 1: Use the acceptance test to guide your progress towards the solution

Here's the specification for an acceptance test that expresses the desired behaviour for this

Given a client makes a deposit of 1000 on 10-01-2012

And a deposit of 2000 on 13-01-2012

And a withdrawal of 500 on 14-01-2012

When they print their bank statement

Then they would see:
```
Date       || Amount || Balance
14/01/2012 || -500   || 2500
13/01/2012 || 2000   || 3000
10/01/2012 || 1000   || 1000
```
### Tip 2: When in doubt, go for the simplest solution!

### About solution

**USER STORY 1:** Create account
```
In order to save money
As a natural person
I want to create a new account in my favorite bank
```
> POST /v1.0/account

**USER STORY 2:** Deposit money
```
In order to save money
As a bank client
I want to make a deposit in my account
```
> POST /v1.0/account/deposits

**USER STORY 3:** Withdrawal money
```
In order to retrieve some or all of my savings
As a bank client
I want to make a withdrawal from my account
```
> POST /v1.0/account/withdrawals

**USER STORY 4:** Print history
```
In order to check my operations
As a bank client
I want to see the history (date, amount, balance) of my operations
```
> GET /v1.0/account/statements

**NOTES**

API is working in a SQL Repository. In test case, transactions and account are saved in an inMemory Repository.
