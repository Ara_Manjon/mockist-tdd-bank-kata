package com.bankata.api.service;

import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public interface ITimeServer {
    Timestamp getDate();
}
