package com.bankata.api.service;

import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;

@Component
public class TimeServer implements ITimeServer {
    @Override
    public Timestamp getDate() {
        return new Timestamp(new Date().getTime());
    }
}
