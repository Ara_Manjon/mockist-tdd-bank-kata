package com.bankata.api.service;

public interface IGeneratorID {
  String generate();

}
