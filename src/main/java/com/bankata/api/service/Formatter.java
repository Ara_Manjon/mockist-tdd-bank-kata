package com.bankata.api.service;

import com.bankata.api.domain.Statement;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

@Component
public
class Formatter implements IFormatter {

  @Override
  public String print(Statement statement) {
    String statementFormatted = "date       | amount  | balance\n";
    for (int i = statement.statement.size()-1; i >= 0 ; i--) {
      String separator = " | ";
      statementFormatted = statementFormatted
              .concat(formatDate(statement.statement.get(i).date))
              .concat(separator)
              .concat(formatAmount(toPositive(statement.statement.get(i).amount)))
              .concat(separator)
              .concat(formatAmount(statement.statement.get(i).balance))
              .concat("\n");
    }
    return statementFormatted;
  }

  private String formatDate(Timestamp date) {
    return new SimpleDateFormat("dd-MM-yyyy").format(date);
  }

  private float toPositive(float amount) {
    if (amount < 0) return -amount;
    return amount;
  }

  private String formatAmount(float amount) {
    NumberFormat numberFormat = new DecimalFormat("#00.00");
    return formatSpaces(numberFormat.format(amount));
  }

  private String formatSpaces(String amount) {
    if (amount.length() == 5) return  "  " +amount;
    if (amount.length() == 6) return  " " +amount;
    return amount;
  }

}
