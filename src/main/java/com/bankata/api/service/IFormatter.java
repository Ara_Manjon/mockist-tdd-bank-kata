package com.bankata.api.service;

import com.bankata.api.domain.Statement;

public interface IFormatter {
    String print(Statement statement);
}
