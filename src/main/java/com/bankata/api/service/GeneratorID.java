package com.bankata.api.service;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class GeneratorID implements IGeneratorID {
  public String generate(){

    return UUID.randomUUID().toString();
  }
}
