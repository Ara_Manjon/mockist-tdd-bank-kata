package com.bankata.api.infrastructure;

import com.bankata.api.domain.entities.transactions.Transaction;
import com.bankata.api.domain.entities.transactions.Transactions;
import com.bankata.api.domain.entities.valueobjects.Id;

public interface ITransactionRepository {

  void add(Transaction transaction, Id id);

  Transactions findAll(Id id);
}
