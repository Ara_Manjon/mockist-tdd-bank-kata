package com.bankata.api.infrastructure;

import com.bankata.api.domain.entities.accounts.Account;
import com.bankata.api.domain.entities.valueobjects.Id;

public interface IAccountRepository {
  Account getById(Id id);

  void add(Account account);
}
