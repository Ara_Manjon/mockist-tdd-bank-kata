package com.bankata.api.infrastructure.sql;

import com.bankata.api.domain.entities.accounts.Account;
import com.bankata.api.domain.entities.AccountDTO;
import com.bankata.api.domain.entities.TransactionDTO;
import com.bankata.api.domain.entities.valueobjects.Id;
import com.bankata.api.infrastructure.IAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import com.bankata.api.domain.application.exceptions.NotAllowedCreateAccountException;
import com.bankata.api.domain.application.exceptions.NotFoundAccountException;


@Primary
@Component
public class SQLAccountRepository implements IAccountRepository {

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Override
  public void add(Account account) {
    try {
      AccountDTO accountDTO = account.serialize();
      jdbcTemplate.update("INSERT INTO accounts (id) VALUES (?)", accountDTO.id);
      for (TransactionDTO transactionDTO : accountDTO.transactions) {
        jdbcTemplate.update("INSERT INTO transactions (id, amount, transaction_date) VALUES (?, ?, ?)", accountDTO.id, transactionDTO.amount, transactionDTO.date);
      }
    }catch (DuplicateKeyException e) {
      throw new NotAllowedCreateAccountException();
    }

  }

  @Override
  public Account getById(Id id) {
    try {
      AccountDTO accountDTO = new AccountDTO();
      jdbcTemplate.queryForObject("SELECT * FROM accounts WHERE id=?",
              new Object[]{id.serialize()},
              (rs, rowNum) -> {
                accountDTO.id = rs.getString("id");
                return null;
              });
      return Account.deserialize(accountDTO);
    } catch (EmptyResultDataAccessException e) {
      throw new NotFoundAccountException();
    }
  }
}