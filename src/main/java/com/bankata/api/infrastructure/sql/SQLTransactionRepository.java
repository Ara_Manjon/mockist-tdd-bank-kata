package com.bankata.api.infrastructure.sql;

import com.bankata.api.domain.entities.TransactionDTO;
import com.bankata.api.domain.entities.dto.TransactionsDTO;
import com.bankata.api.domain.entities.transactions.Transaction;
import com.bankata.api.domain.entities.transactions.Transactions;
import com.bankata.api.domain.entities.valueobjects.Id;
import com.bankata.api.infrastructure.ITransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Primary
@Component
public class SQLTransactionRepository implements ITransactionRepository {

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Override
  public void add(Transaction transaction, Id id) {
    TransactionDTO transactionDTO = transaction.serialize();
    jdbcTemplate.update("INSERT INTO transactions (id, amount, transaction_date) VALUES (?, ?, ?)", id.serialize(), transactionDTO.amount, transactionDTO.date);
  }

  @Override
  public Transactions findAll(Id id) {
    TransactionsDTO transactionsDTO = new TransactionsDTO();
    jdbcTemplate.query("SELECT * FROM transactions WHERE id=? ORDER BY transaction_date ASC",
            new Object[]{id.serialize()},
            (rs, rowNum) -> {
              TransactionDTO transactionDTO = new TransactionDTO();
              transactionDTO.amount = rs.getString("amount");
              transactionDTO.date = rs.getTimestamp("transaction_date");
              transactionsDTO.transactions.add(transactionDTO);
              return null;
            });
    return Transactions.deserialize(transactionsDTO);
  }

}
