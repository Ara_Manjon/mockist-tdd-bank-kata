package com.bankata.api.infrastructure.inmemory;

import com.bankata.api.domain.entities.TransactionDTO;
import com.bankata.api.domain.entities.dto.TransactionsDTO;
import com.bankata.api.domain.entities.transactions.Transaction;
import com.bankata.api.domain.entities.transactions.Transactions;
import com.bankata.api.domain.entities.valueobjects.Id;
import com.bankata.api.infrastructure.ITransactionRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class InMemoryTransactionRepository implements ITransactionRepository {
  Map<Id, List<TransactionDTO>> transactions = new HashMap<>();

  @Override
  public Transactions findAll(Id id) {
    TransactionsDTO transactionsDTO = new TransactionsDTO();
    List<TransactionDTO> transactionsById = transactions.get(id);
    if(transactionsById == null) transactionsById = new ArrayList<>();
    for (TransactionDTO transactionDTO : transactionsById) {
      transactionsDTO.add(transactionDTO);
    }
    return Transactions.deserialize(transactionsDTO);
  }

  @Override
  public void add(Transaction transaction, Id id) {
    TransactionDTO transactionDTO = transaction.serialize();
    List<TransactionDTO> transactionsById = transactions.get(id);
    if (transactionsById == null) {
      transactionsById = new ArrayList<>();
    }
    transactionsById.add(transactionDTO);
    transactions.put(id, transactionsById);
  }

}
