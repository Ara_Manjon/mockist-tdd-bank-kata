package com.bankata.api.infrastructure.inmemory;

import com.bankata.api.domain.entities.accounts.Account;
import com.bankata.api.domain.entities.AccountDTO;
import com.bankata.api.domain.application.exceptions.NotAllowedCreateAccountException;
import com.bankata.api.domain.application.exceptions.NotFoundAccountException;
import com.bankata.api.domain.entities.valueobjects.Id;
import com.bankata.api.infrastructure.IAccountRepository;

import java.util.ArrayList;
import java.util.List;

public class InMemoryAccountRepository implements IAccountRepository {

  List<String> accounts = new ArrayList<>();
  @Override
  public void add(Account account) throws NotAllowedCreateAccountException {
    AccountDTO accountDTO = account.serialize();
    if (accounts.contains(accountDTO.id)) throw new NotAllowedCreateAccountException();
    accounts.add(accountDTO.id);
  }

  @Override
  public Account getById(Id id) throws NotFoundAccountException {
    String idDTO = id.serialize();
    if(!accounts.contains(idDTO)) throw new NotFoundAccountException();
    AccountDTO accountDTO = new AccountDTO();
    accountDTO.id = idDTO;
    return Account.deserialize(accountDTO);
  }
}
