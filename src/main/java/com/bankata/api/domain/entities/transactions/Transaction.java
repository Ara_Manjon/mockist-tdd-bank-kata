package com.bankata.api.domain.entities.transactions;

import com.bankata.api.domain.entities.TransactionDTO;
import com.bankata.api.domain.entities.valueobjects.Date;
import com.bankata.api.domain.StatementLine;

import java.util.Objects;

public class Transaction {
  private final Amount amount;
  private final Date date;

  public Transaction(Amount amount, Date date) {
    this.amount = amount;
    this.date = date;
  }

  public StatementLine createLine(Amount currentBalance) {
    StatementLine statementLine = new StatementLine();
    statementLine.amount = Float.parseFloat(this.amount.serialize().amount);
    statementLine.date = this.date.serialize().date;
    statementLine.balance = currentBalance.calculate(Float.parseFloat(this.amount.serialize().amount));
    return statementLine;
  }

  public TransactionDTO serialize() {
    TransactionDTO transactionDTO = new TransactionDTO();
    transactionDTO.amount = this.amount.serialize().amount;
    transactionDTO.date = this.date.serialize().date;
    return transactionDTO;
  }

  public static Transaction deserialize(TransactionDTO transactionDTO) {
    return new Transaction(new Amount(Float.parseFloat(transactionDTO.amount)), new Date(transactionDTO.date));
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Transaction that = (Transaction) o;
    return Objects.equals(amount, that.amount) &&
            Objects.equals(date, that.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(amount, date);
  }

  @Override
  public String toString() {
    return "Transaction{" +
            "amount=" + amount +
            ", date=" + date +
            '}';
  }
}
