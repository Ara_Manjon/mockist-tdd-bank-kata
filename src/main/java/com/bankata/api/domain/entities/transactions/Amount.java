package com.bankata.api.domain.entities.transactions;

import com.bankata.api.domain.entities.dto.AmountDTO;

import java.util.Objects;

public class Amount {
  private float amount;

  public Amount(float amount) {
    this.amount = amount;
  }

  public float calculate(float amount) {
    return this.amount += amount;
  }
  public Amount toNegative() {
    return new Amount(-amount);
  }

  public AmountDTO serialize() {
    AmountDTO amountDTO = new AmountDTO();
    amountDTO.amount = Float.toString(this.amount);
    return amountDTO;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Amount amount = (Amount) o;
    return Float.compare(amount.amount, this.amount) == 0;
  }

  @Override
  public int hashCode() {
    return Objects.hash(amount);
  }

  @Override
  public String toString() {
    return "Amount{" +
            "amount=" + amount +
            '}';
  }
}
