package com.bankata.api.domain.entities.dto;

import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;

public class DateDTO {
  @NotBlank
  public Timestamp date;
}
