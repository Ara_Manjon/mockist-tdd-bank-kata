package com.bankata.api.domain.entities.accounts;

import com.bankata.api.domain.entities.AccountDTO;
import com.bankata.api.domain.entities.valueobjects.Id;

import java.util.Objects;

public class Account {
  private final Id id;

  public Account(Id id) {
    this.id = id;
  }

  public AccountDTO serialize() {
    AccountDTO accountDTO = new AccountDTO();
    accountDTO.id = id.serialize();
    return accountDTO;
  }

  public static Account deserialize(AccountDTO accountDTO) {
    return new Account(new Id(accountDTO.id));
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Account account = (Account) o;
    return Objects.equals(id, account.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "Account{" +
            "id=" + id +
            '}';
  }
}


