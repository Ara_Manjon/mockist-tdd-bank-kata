package com.bankata.api.domain.entities.dto;

import javax.validation.constraints.NotBlank;

public class IdDTO {
  @NotBlank
  public String id;
}
