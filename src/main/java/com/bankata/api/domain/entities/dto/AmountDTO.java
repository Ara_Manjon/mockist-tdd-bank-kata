package com.bankata.api.domain.entities.dto;

import javax.validation.constraints.NotBlank;

public class AmountDTO {
  @NotBlank
  public String amount;
}