package com.bankata.api.domain.entities.valueobjects;
import com.bankata.api.domain.entities.dto.DateDTO;

import java.sql.Timestamp;
import java.util.Objects;

public class Date {
  private Timestamp date;

  public Date(Timestamp date) {
    this.date = date;
  }

  public DateDTO serialize() {
    DateDTO dateDTO = new DateDTO();
    dateDTO.date = this.date;
    return dateDTO;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Date date1 = (Date) o;
    return Objects.equals(date, date1.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(date);
  }

  @Override
  public String toString() {
    return "Date{" +
            "date=" + date +
            '}';
  }
}
