package com.bankata.api.domain.entities;

import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;
import java.util.Objects;

public class TransactionDTO {
  @NotBlank
  public String amount;
  @NotBlank
  public Timestamp date;
  public float balance;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    TransactionDTO that = (TransactionDTO) o;
    return Objects.equals(amount, that.amount) &&
            Objects.equals(date, that.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(amount, date);
  }

  @Override
  public String toString() {
    return "TransactionDTO{" +
            "amount='" + amount + '\'' +
            ", date=" + date +
            '}';
  }
}
