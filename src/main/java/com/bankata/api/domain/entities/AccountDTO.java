package com.bankata.api.domain.entities;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AccountDTO {
  @NotBlank
  public String id;
  @NotBlank
  public List<TransactionDTO> transactions = new ArrayList<>();

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AccountDTO that = (AccountDTO) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "AccountDTO{" +
            "id='" + id + '\'' +
            ", transactions=" + transactions +
            '}';
  }
}
