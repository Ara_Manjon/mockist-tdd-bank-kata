package com.bankata.api.domain.entities.dto;


import com.bankata.api.domain.entities.TransactionDTO;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TransactionsDTO implements Iterable<TransactionDTO> {
  public List<TransactionDTO> transactions;

  public TransactionsDTO() {
    this.transactions = new ArrayList<>();
  }

  public void add(TransactionDTO transactionDTO) {
    transactions.add(transactionDTO);
  }

  @Override
  public Iterator<TransactionDTO> iterator() {
    return this.transactions.iterator();
  }
}



