package com.bankata.api.domain.entities.valueobjects;
import com.bankata.api.domain.entities.dto.IdDTO;

import java.util.Objects;

public class Id {
  private String id;

  public Id(String id) {
    this.id = id;
  }

  public String serialize() {
    IdDTO idDTO = new IdDTO();
    idDTO.id = this.id;
    return id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Id id1 = (Id) o;
    return Objects.equals(id, id1.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "Id{" +
            "id='" + id + '\'' +
            '}';
  }
}
