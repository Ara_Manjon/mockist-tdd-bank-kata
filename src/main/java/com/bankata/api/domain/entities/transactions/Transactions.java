package com.bankata.api.domain.entities.transactions;

import com.bankata.api.domain.entities.TransactionDTO;
import com.bankata.api.domain.entities.dto.TransactionsDTO;
import com.bankata.api.domain.Statement;
import com.bankata.api.domain.StatementLine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class Transactions implements Iterable<Transaction>{
  private final List<Transaction> transactions;

  public Transactions() {
    this.transactions = new ArrayList<>();

  }

  public Statement createStatement() {
    Amount currentBalance = new Amount(0);
    Statement statement = new Statement();
    for (Transaction transaction : transactions) {
      StatementLine statementLine = transaction.createLine(currentBalance);
      currentBalance = new Amount(statementLine.balance);
      statement.add(statementLine);
    }
    return statement;
  }

  public static Transactions serialize(List<TransactionDTO> transactionListDTO) {
    Transactions transactions = new Transactions();
    if(transactionListDTO == null) return transactions;
    for (TransactionDTO transactionDTO : transactionListDTO) {
      transactions.add(Transaction.deserialize(transactionDTO));
    }
    return transactions;
  }

  public static Transactions deserialize(TransactionsDTO transactionsDTO) {
    Transactions transactions = new Transactions();
    for (TransactionDTO transaction : transactionsDTO) {
      transactions.add(Transaction.deserialize(transaction));
    }
    return transactions;
  }

  public void add(Transaction transaction) {
    this.transactions.add(transaction);
  }


  @Override
  public Iterator<Transaction> iterator() {
    return this.transactions.iterator();
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Transactions that = (Transactions) o;
    return Objects.equals(transactions, that.transactions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transactions);
  }

  @Override
  public String toString() {
    return "Transactions{" +
            "transactions=" + transactions +
            '}';
  }
}
