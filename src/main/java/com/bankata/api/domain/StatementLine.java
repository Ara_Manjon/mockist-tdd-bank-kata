package com.bankata.api.domain;


import java.sql.Timestamp;
import java.util.Objects;

public class StatementLine {
  public float amount;
  public Timestamp date;
  public float balance;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    StatementLine that = (StatementLine) o;
    return Objects.equals(amount, that.amount) &&
            Objects.equals(date, that.date) &&
            Objects.equals(balance, that.balance);
  }

  @Override
  public int hashCode() {
    return Objects.hash(amount, date, balance);
  }

}
