package com.bankata.api.domain.application;

import com.bankata.api.domain.entities.transactions.Amount;
import com.bankata.api.domain.entities.transactions.Transaction;
import com.bankata.api.domain.entities.transactions.Transactions;
import com.bankata.api.domain.entities.valueobjects.Date;
import com.bankata.api.domain.entities.valueobjects.Id;
import com.bankata.api.infrastructure.ITransactionRepository;
import com.bankata.api.service.IFormatter;
import com.bankata.api.service.ITimeServer;
import org.springframework.stereotype.Service;

@Service
public class TransactionsService {
  private final ITransactionRepository repository;
  private final ITimeServer timeServer;
  private final IFormatter formatter;

  public TransactionsService(ITransactionRepository repository, ITimeServer timeServer, IFormatter formatter) {
    this.repository = repository;
    this.timeServer = timeServer;
    this.formatter = formatter;
  }

  public void deposit(Amount amount, Id id) {
    Transaction transaction = new Transaction(amount, new Date(timeServer.getDate()));
    repository.add(transaction, id);
  }

  public void withdraw(Amount amount, Id id) {
    Transaction transaction = new Transaction(amount.toNegative(), new Date(timeServer.getDate()));
    repository.add(transaction, id);
  }

  public String print(Id id) {
    Transactions transactions = repository.findAll(id);
    return formatter.print(transactions.createStatement());
  }
}
