package com.bankata.api.domain.application.exceptions;


public class NotFoundAccountException extends AccountException {

  public NotFoundAccountException() {
    this.message = "Not found Account";
  }

}
