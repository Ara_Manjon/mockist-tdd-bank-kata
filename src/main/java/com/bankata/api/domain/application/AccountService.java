package com.bankata.api.domain.application;

import com.bankata.api.domain.entities.accounts.Account;
import com.bankata.api.domain.entities.AccountDTO;
import com.bankata.api.domain.application.exceptions.NotAllowedCreateAccountException;
import com.bankata.api.domain.application.exceptions.NotFoundAccountException;
import com.bankata.api.domain.entities.valueobjects.Id;
import com.bankata.api.infrastructure.IAccountRepository;
import com.bankata.api.service.IGeneratorID;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
  private IAccountRepository accountRepository;
  private IGeneratorID generatorID;

  public AccountService(IAccountRepository accountRepository, IGeneratorID generatorID) {
    this.accountRepository = accountRepository;
    this.generatorID = generatorID;
  }

  public AccountDTO createAccount() {
    Id id = new Id(generatorID.generate());
    try {
      accountRepository.add(new Account(id));
    } catch (NotAllowedCreateAccountException exception) {
      throw new NotAllowedCreateAccountException();
    }
    return retrieveAccount(id);
  }

  public AccountDTO retrieveAccount(Id id) {
    try {
      return accountRepository.getById(id).serialize();
    } catch (NotFoundAccountException exception) {
      throw new NotFoundAccountException();
    }
  }
}
