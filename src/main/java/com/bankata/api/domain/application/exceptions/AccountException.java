package com.bankata.api.domain.application.exceptions;

public class AccountException extends RuntimeException{
  protected String message;

  @Override
  public String getMessage() {
    return message;
  }
}
