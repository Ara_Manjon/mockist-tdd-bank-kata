package com.bankata.api.domain.application.exceptions;

public class NotAllowedCreateAccountException extends AccountException {

  public NotAllowedCreateAccountException() {
    this.message = "Not allowed create an Account";
  }

}
