package com.bankata.api.controllers;

import com.bankata.api.controllers.useCase.BankUseCase;
import com.bankata.api.controllers.useCase.IdUseCase;
import com.bankata.api.domain.application.TransactionsService;
import com.bankata.api.domain.entities.transactions.Amount;
import com.bankata.api.domain.entities.valueobjects.Id;
import com.bankata.api.infrastructure.ITransactionRepository;
import com.bankata.api.service.IFormatter;
import com.bankata.api.service.ITimeServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionController {

  @Autowired
  ITransactionRepository SQLTransactionRepository;
  @Autowired
  ITimeServer timeServer;
  @Autowired
  IFormatter formatter;

  @PostMapping(value = "account/deposits", consumes = "application/json")
  public ResponseEntity<Object> deposit(@RequestBody BankUseCase bankUseCase) {
    TransactionsService transactionsService = new TransactionsService(SQLTransactionRepository, timeServer, formatter);
    try {
      transactionsService.deposit(new Amount(bankUseCase.amount), new Id(bankUseCase.id));
      return new ResponseEntity<>("Deposit Transaction 👍🏻", HttpStatus.CREATED);
    } catch (Exception error) {
      return new ResponseEntity<>("Service Unavailable", HttpStatus.SERVICE_UNAVAILABLE);
    }
  }

  @PostMapping(value = "account/withdrawals", consumes = "application/json")
  public ResponseEntity<Object> withdraw(@RequestBody BankUseCase bankUseCase) {
    TransactionsService transactionsService = new TransactionsService(SQLTransactionRepository, timeServer, formatter);
    try {
      transactionsService.withdraw(new Amount(bankUseCase.amount), new Id(bankUseCase.id));
      return new ResponseEntity<>("Withdraw Transaction 👍🏻", HttpStatus.CREATED);
    }  catch (Exception error) {
      return new ResponseEntity<>("Service Unavailable", HttpStatus.SERVICE_UNAVAILABLE);
    }
  }

  @GetMapping(value = "account/statements", consumes = "application/json", produces = "application/json")
  public ResponseEntity<Object> printStatement(@RequestBody IdUseCase idUseCase) {
    TransactionsService transactionsService = new TransactionsService(SQLTransactionRepository, timeServer, formatter);
    try {
      return new ResponseEntity<>(transactionsService.print(new Id(idUseCase.id)), HttpStatus.OK);
    } catch (Exception error) {
      return new ResponseEntity<>("Service Unavailable", HttpStatus.SERVICE_UNAVAILABLE);
    }
  }
}
