package com.bankata.api.controllers;

import com.bankata.api.domain.application.exceptions.AccountException;
import com.bankata.api.infrastructure.IAccountRepository;
import com.bankata.api.service.IGeneratorID;
import com.bankata.api.domain.application.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

  @Autowired
  IAccountRepository SQLAccountRepository;

  @Autowired
  IGeneratorID generatorID;

  @PostMapping(value = "/account")
  public ResponseEntity<Object> account() {
    AccountService accountService = new AccountService(SQLAccountRepository, generatorID);
    try {
      return new ResponseEntity<>(accountService.createAccount(), HttpStatus.CREATED);
    } catch (AccountException error) {
      return new ResponseEntity<>(error.getMessage(), HttpStatus.BAD_REQUEST);
    } catch (Exception error) {
      return new ResponseEntity<>("Service Unavailable", HttpStatus.SERVICE_UNAVAILABLE);
    }
  }
}
