package com.bankata.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankKataApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankKataApiApplication.class, args);
	}

}
