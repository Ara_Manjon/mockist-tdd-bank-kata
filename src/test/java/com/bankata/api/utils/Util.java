package com.bankata.api.utils;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {
  public static Timestamp convertStringToTimestamp(String dateString) {
    try {
      DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
      Date date = formatter.parse(dateString);

      return new Timestamp(date.getTime());
    } catch (ParseException e) {
      return null;
    }
  }
}

