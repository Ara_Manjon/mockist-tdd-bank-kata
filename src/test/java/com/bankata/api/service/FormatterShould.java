package com.bankata.api.service;
import com.bankata.api.domain.Statement;
import com.bankata.api.utils.Util;
import com.bankata.api.domain.StatementLine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FormatterShould {

  @Test
  public void print_statement_with_format() {
    Formatter formatter = new Formatter();
    Statement statement = new Statement();
    StatementLine statementLine = new StatementLine();
    statementLine.amount = 1000;
    statementLine.date = Util.convertStringToTimestamp("22-08-2014");
    statementLine.balance = 1100;
    StatementLine statementLine2 = new StatementLine();
    statementLine2.amount = -100;
    statementLine2.date = Util.convertStringToTimestamp("23-08-2014");
    statementLine2.balance = 1000;
    statement.statement.add(statementLine);
    statement.statement.add(statementLine2);
    String expected = "date       | amount  | balance\n" +
            "23-08-2014 |  100.00 | 1000.00\n" +
            "22-08-2014 | 1000.00 | 1100.00\n";

    String result = formatter.print(statement);

    Assertions.assertEquals(expected, result);
  }
}
