package com.bankata.api.infraestructure;

import com.bankata.api.domain.application.exceptions.NotAllowedCreateAccountException;
import com.bankata.api.domain.application.exceptions.NotFoundAccountException;
import com.bankata.api.domain.entities.accounts.Account;
import com.bankata.api.domain.entities.valueobjects.Id;
import com.bankata.api.infrastructure.sql.SQLAccountRepository;
import com.bankata.api.service.GeneratorID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql(scripts = {"/schema.sql"})
public class SQLAccountRepositoryShould {
  @Autowired
  SQLAccountRepository SQLAccountRepository;

  @Test
  public void create_and_retrieve_an_account_correctly() {
    GeneratorID generatorID = new GeneratorID();
    String id = generatorID.generate();
    Account account = new Account(new Id(id));

    SQLAccountRepository.add(account);

    Assertions.assertEquals(account, SQLAccountRepository.getById(new Id(id)));
  }

  @Test
  public void throw_an_error_if_is_not_found_an_account_by_id() {
    GeneratorID generatorID = new GeneratorID();
    String id = generatorID.generate();
    Account account = new Account(new Id(id));

    SQLAccountRepository.add(account);

    Assertions.assertThrows(NotFoundAccountException.class, () -> SQLAccountRepository.getById(new Id("1998")));
  }

  @Test
  public void throw_an_error_if_is_key_id_is_duplicated() {
    GeneratorID generatorID = new GeneratorID();
    String id = generatorID.generate();
    Account account = new Account(new Id(id));
    Account account2 = new Account(new Id(id));
    SQLAccountRepository.add(account);

    Assertions.assertThrows(NotAllowedCreateAccountException.class, () -> SQLAccountRepository.add(account2));
  }
}
