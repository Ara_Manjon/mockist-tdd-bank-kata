package com.bankata.api.infraestructure;

import com.bankata.api.domain.entities.transactions.Amount;
import com.bankata.api.domain.entities.transactions.Transaction;
import com.bankata.api.domain.entities.transactions.Transactions;
import com.bankata.api.domain.entities.valueobjects.Date;
import com.bankata.api.domain.entities.valueobjects.Id;
import com.bankata.api.infrastructure.sql.SQLTransactionRepository;
import com.bankata.api.service.GeneratorID;
import com.bankata.api.utils.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql(scripts = {"/schema.sql"})
public class SQLTransactionRepositoryShould {

  @Autowired
  SQLTransactionRepository SQLTransactionRepository;

  @Test
  public void save_and_retrieve_all_transactions_correctly() {
    GeneratorID generatorID = new GeneratorID();
    String id = generatorID.generate();
    Transaction transaction = new Transaction(new Amount(1000), new Date(Util.convertStringToTimestamp("22-08-2014")));
    Transaction transaction2 = new Transaction(new Amount(100), new Date(Util.convertStringToTimestamp("23-08-2014")));
    Transactions transactions = new Transactions();
    transactions.add(transaction);
    transactions.add(transaction2);

    SQLTransactionRepository.add(transaction, new Id(id));
    SQLTransactionRepository.add(transaction2, new Id(id));

    Assertions.assertEquals(transactions, SQLTransactionRepository.findAll(new Id(id)));

  }
}
