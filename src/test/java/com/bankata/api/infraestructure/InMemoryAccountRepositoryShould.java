package com.bankata.api.infraestructure;


import com.bankata.api.domain.application.exceptions.NotAllowedCreateAccountException;
import com.bankata.api.domain.application.exceptions.NotFoundAccountException;
import com.bankata.api.domain.entities.accounts.Account;
import com.bankata.api.domain.entities.valueobjects.Id;
import com.bankata.api.infrastructure.IAccountRepository;
import com.bankata.api.infrastructure.inmemory.InMemoryAccountRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class InMemoryAccountRepositoryShould {

  @Test
  public void add_an_account_correctly() {
    IAccountRepository repository = new InMemoryAccountRepository();
    Account account = new Account(new Id("199"));
    repository.add(account);

    Assertions.assertEquals(account, repository.getById(new Id("199")));
  }

  @Test
  public void throw_an_error_if_is_not_allowed_create_an_account() {
    IAccountRepository repository = new InMemoryAccountRepository();
    Account account = new Account(new Id("199"));

    repository.add(account);

    Assertions.assertThrows(NotAllowedCreateAccountException.class, ()-> repository.add(account));
  }

  @Test
  public void throw_an_error_if_is_not_found_an_account() {
    IAccountRepository repository = new InMemoryAccountRepository();

    Assertions.assertThrows(NotFoundAccountException.class, ()-> repository.getById(new Id("122")));
  }
}
