package com.bankata.api.infraestructure;

import com.bankata.api.domain.entities.transactions.Amount;
import com.bankata.api.domain.entities.transactions.Transaction;
import com.bankata.api.domain.entities.transactions.Transactions;
import com.bankata.api.domain.entities.valueobjects.Date;
import com.bankata.api.domain.entities.valueobjects.Id;
import com.bankata.api.infrastructure.inmemory.InMemoryTransactionRepository;
import com.bankata.api.utils.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class InMemoryTransactionRepositoryShould {

  @Test
  public void save_an_retrieve_transaction_correctly() {

    InMemoryTransactionRepository repository = new InMemoryTransactionRepository();
    Transaction transaction = new Transaction(new Amount(1000), new Date(Util.convertStringToTimestamp("22-08-2014")));
    Transaction transaction2 = new Transaction(new Amount(100), new Date(Util.convertStringToTimestamp("23-08-2014")));
    Transactions transactions = new Transactions();
    transactions.add(transaction);
    transactions.add(transaction2);

    repository.add(transaction, new Id("199"));
    repository.add(transaction2, new Id("199"));

    Assertions.assertEquals(transactions, repository.findAll(new Id("199")));
  }
}
