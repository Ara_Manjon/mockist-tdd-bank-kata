package com.bankata.api.acceptance;
import com.bankata.api.domain.entities.transactions.Amount;
import com.bankata.api.domain.entities.valueobjects.Id;
import com.bankata.api.domain.application.TransactionsService;
import com.bankata.api.service.Formatter;
import com.bankata.api.service.IFormatter;
import com.bankata.api.service.TimeServer;
import com.bankata.api.utils.Util;
import com.bankata.api.infrastructure.ITransactionRepository;
import com.bankata.api.infrastructure.inmemory.InMemoryTransactionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

public class PrintStatementShould {

    private ITransactionRepository inMemoryTransactionRepository = new InMemoryTransactionRepository();
    private IFormatter formatter = new Formatter();
    @Mock
    private TimeServer timeServer;


    @Test
    public void print_the_bank_Statement_correctly() {
        MockitoAnnotations.initMocks(this);
        TransactionsService transactionsService = new TransactionsService(inMemoryTransactionRepository, timeServer, formatter);
        when(timeServer.getDate()).thenReturn(Util.convertStringToTimestamp("10-01-2012"), Util.convertStringToTimestamp("13-01-2012"), Util.convertStringToTimestamp("14-01-2012"));
        String expected = "date       | amount  | balance\n" +
                "14-01-2012 |  500.00 | 2500.00\n" +
                "13-01-2012 | 2000.00 | 3000.00\n" +
                "10-01-2012 | 1000.00 | 1000.00\n";

        transactionsService.deposit(new Amount(1000), new Id("199"));
        transactionsService.deposit(new Amount(2000), new Id("199"));
        transactionsService.withdraw(new Amount(500),new Id("199"));
        String result = transactionsService.print(new Id("199"));

        Assertions.assertEquals(expected, result);
    }
}
