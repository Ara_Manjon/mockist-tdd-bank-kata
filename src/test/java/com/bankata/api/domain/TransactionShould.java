package com.bankata.api.domain;
import com.bankata.api.domain.entities.TransactionDTO;
import com.bankata.api.domain.entities.transactions.Amount;
import com.bankata.api.domain.entities.transactions.Transaction;
import com.bankata.api.domain.entities.valueobjects.Date;
import com.bankata.api.utils.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TransactionShould {

  @Test
  public void create_a_description_from_withdrawal_transaction_correctly() {
    Date date = new Date(Util.convertStringToTimestamp("22-08-2014"));
    Transaction withdrawal = new Transaction(new Amount(-100), date);
    TransactionDTO expected = new TransactionDTO();
    expected.amount = Float.toString(-100);
    expected.date = Util.convertStringToTimestamp("22-08-2014");

    Assertions.assertEquals(expected, withdrawal.serialize());
  }

  @Test
  public void create_a_description_from_deposit_transaction_correctly() {
    Date date = new Date(Util.convertStringToTimestamp("22-08-2014"));
    Transaction deposit = new Transaction(new Amount(100), date);
    TransactionDTO expected = new TransactionDTO();
    expected.amount = Float.toString(100);
    expected.date = Util.convertStringToTimestamp("22-08-2014");

    Assertions.assertEquals(expected, deposit.serialize());
  }

  @Test
  public void create_a_line_statement_correctly() {
    Amount balance = new Amount(0);
    Transaction transaction = new Transaction(new Amount(1000), new Date(Util.convertStringToTimestamp("22-08-2014")));
    StatementLine statementLine = new StatementLine();
    statementLine.amount = 1000;
    statementLine.date = Util.convertStringToTimestamp("22-08-2014");
    statementLine.balance = 1000;

    Assertions.assertEquals(statementLine, transaction.createLine(balance));
  }

}
