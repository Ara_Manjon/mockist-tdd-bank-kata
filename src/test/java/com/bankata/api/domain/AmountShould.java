package com.bankata.api.domain;

import com.bankata.api.domain.entities.transactions.Amount;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class AmountShould {

  @Test
  public void calculate_amount_between_two_amounts() {
    Amount amount = new Amount(100);

    Assertions.assertEquals(50, amount.calculate(-50));
  }
}
