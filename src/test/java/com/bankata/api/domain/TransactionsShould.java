package com.bankata.api.domain;

import com.bankata.api.domain.entities.transactions.Amount;
import com.bankata.api.domain.entities.transactions.Transaction;
import com.bankata.api.domain.entities.transactions.Transactions;
import com.bankata.api.domain.entities.valueobjects.Date;
import com.bankata.api.utils.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TransactionsShould {

  @Test
  public void create_a_statement_correctly() {
    Transactions transactions = new Transactions();
    transactions.add(new Transaction(new Amount(1000), new Date(Util.convertStringToTimestamp("22-08-2014"))));
    transactions.add(new Transaction(new Amount(-100), new Date(Util.convertStringToTimestamp("23-08-2014"))));
    Statement expected = new Statement();
    StatementLine statementLine = new StatementLine();
    statementLine.amount = 1000;
    statementLine.date = Util.convertStringToTimestamp("22-08-2014");
    statementLine.balance = 1000;
    StatementLine statementLine2 = new StatementLine();
    statementLine2.amount = -100;
    statementLine2.date = Util.convertStringToTimestamp("23-08-2014");
    statementLine2.balance = 900;
    expected.statement.add(statementLine);
    expected.statement.add(statementLine2);

    Assertions.assertEquals(expected, transactions.createStatement());
  }
}
