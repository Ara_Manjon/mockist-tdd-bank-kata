package com.bankata.api.controllers;

import com.bankata.api.domain.entities.AccountDTO;
import com.bankata.api.infrastructure.IAccountRepository;
import com.bankata.api.service.GeneratorID;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class AccountControllerEndToEndShould {

  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private IAccountRepository SQLAccountRepository;
  @Autowired
  private ObjectMapper objectMapper;
  @MockBean
  private GeneratorID generatorID;

  @Test
  public void create_an_account_correctly() throws Exception {
    MockitoAnnotations.initMocks(this);
    GeneratorID generatorIDTest = new GeneratorID();
    AccountDTO expected = new AccountDTO();
    String id = generatorIDTest.generate();
    expected.id = id;
    when(generatorID.generate()).thenReturn(id);
    MvcResult result = this.mockMvc.perform(post("/account"))
            .andDo(print())
            .andExpect(status().isCreated())
            .andReturn();
    String resultAsString = result.getResponse().getContentAsString();
    AccountDTO accountResult = objectMapper.readValue(resultAsString, AccountDTO.class);

    Assertions.assertEquals(expected, accountResult);
  }

}
