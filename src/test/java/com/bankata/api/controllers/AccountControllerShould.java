package com.bankata.api.controllers;

import com.bankata.api.domain.entities.accounts.Account;
import com.bankata.api.domain.application.exceptions.NotFoundAccountException;
import com.bankata.api.domain.entities.valueobjects.Id;
import com.bankata.api.infrastructure.IAccountRepository;
import com.bankata.api.service.IGeneratorID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountControllerShould {

  @Mock
  private IAccountRepository inMemoryAccountRepository;

  @Mock
  private IGeneratorID idGenerator;

  @InjectMocks
  private AccountController accountController;

  @Test
  public void create_an_account() {
    MockitoAnnotations.initMocks(this);
    when(idGenerator.generate()).thenReturn("199");
    Account account = new Account(new Id("199"));
    when(inMemoryAccountRepository.getById(new Id("199"))).thenReturn(account);
    ResponseEntity<Object> response = accountController.account();

    Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode());
    Assertions.assertNotNull(response);
  }

  @Test
  public void throw_an_error_when_error_on_bad_request() {
    MockitoAnnotations.initMocks(this);
    when(idGenerator.generate()).thenReturn("199");
    when(inMemoryAccountRepository.getById(new Id("199"))).thenThrow(NotFoundAccountException.class);

    ResponseEntity<Object> response = accountController.account();

    Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    Assertions.assertEquals("Not found Account", response.getBody());
  }
}
