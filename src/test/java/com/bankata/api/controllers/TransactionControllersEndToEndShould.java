package com.bankata.api.controllers;

import com.bankata.api.controllers.useCase.BankUseCase;
import com.bankata.api.controllers.useCase.IdUseCase;
import com.bankata.api.domain.Statement;
import com.bankata.api.infrastructure.ITransactionRepository;
import com.bankata.api.service.Formatter;
import com.bankata.api.service.GeneratorID;
import com.bankata.api.service.IFormatter;
import com.bankata.api.service.ITimeServer;
import com.bankata.api.utils.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TransactionControllersEndToEndShould {
  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private ITransactionRepository SQLTransactionRepository;
  @Autowired
  private ObjectMapper objectMapper;
  @Autowired
  IFormatter formatter;
  @MockBean
  private ITimeServer timeServer;

  @Test
  public void create_a_transaction_correctly() throws Exception {
    MockitoAnnotations.initMocks(this);
    BankUseCase bankUseCase = new BankUseCase();
    GeneratorID generatorIDTest = new GeneratorID();
    bankUseCase.amount = 1000;
    bankUseCase.id = generatorIDTest.generate();
    when(timeServer.getDate()).thenReturn(Util.convertStringToTimestamp("22-08-2014"));
    String jsonRequest = new ObjectMapper().writeValueAsString(bankUseCase);
    MvcResult result = this.mockMvc.perform(post("/account/deposits").contentType(MediaType.APPLICATION_JSON)
            .content(jsonRequest)).andDo(print()).andExpect(status().isCreated())
            .andReturn();

    String message = result.getResponse().getContentAsString();

    Assert.assertEquals("Deposit Transaction 👍🏻", message);
  }

  @Test
  public void create_a_withdraw_transaction_correctly() throws Exception {
    MockitoAnnotations.initMocks(this);
    BankUseCase bankUseCase = new BankUseCase();
    GeneratorID generatorIDTest = new GeneratorID();

    bankUseCase.amount = 1000;
    bankUseCase.id = generatorIDTest.generate();
    when(timeServer.getDate()).thenReturn(Util.convertStringToTimestamp("22-08-2014"));
    String jsonRequest = new ObjectMapper().writeValueAsString(bankUseCase);
    MvcResult result = this.mockMvc.perform(post("/account/withdrawals").contentType(MediaType.APPLICATION_JSON)
            .content(jsonRequest)).andDo(print()).andExpect(status().isCreated())
            .andReturn();

    String message = result.getResponse().getContentAsString();

    Assert.assertEquals("Withdraw Transaction 👍🏻", message);
  }


  @Test
  public void print_statement_correctly() throws Exception {
    MockitoAnnotations.initMocks(this);
    IdUseCase idUseCase = new IdUseCase();
    GeneratorID generatorIDTest = new GeneratorID();
    idUseCase.id = generatorIDTest.generate();
    Statement statement = new Statement();
    Formatter formatterTest = new Formatter();
    when(timeServer.getDate()).thenReturn(Util.convertStringToTimestamp("22-08-2014"));
    String jsonRequest = new ObjectMapper().writeValueAsString(idUseCase);
    MvcResult result = this.mockMvc.perform(get("/account/statements").contentType(MediaType.APPLICATION_JSON)
            .content(jsonRequest)).andDo(print()).andExpect(status().is(200))
            .andReturn();

    String message = result.getResponse().getContentAsString();
    String expected = formatterTest.print(statement);
    
    Assert.assertEquals(expected,message);
  }
}
