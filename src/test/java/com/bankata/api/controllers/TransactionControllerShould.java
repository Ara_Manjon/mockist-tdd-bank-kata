package com.bankata.api.controllers;

import com.bankata.api.controllers.useCase.BankUseCase;
import com.bankata.api.controllers.useCase.IdUseCase;
import com.bankata.api.domain.entities.transactions.Transactions;
import com.bankata.api.domain.entities.valueobjects.Id;
import com.bankata.api.infrastructure.ITransactionRepository;
import com.bankata.api.service.IFormatter;
import com.bankata.api.service.ITimeServer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TransactionControllerShould {
  @Mock
  private ITransactionRepository inMemoryTransactionRepository;
  @Mock
  private IFormatter formatter;
  @Mock
  private ITimeServer timeServer;
  @InjectMocks
  private TransactionController transactionController;

  @Test
  public void create_deposit() {
    MockitoAnnotations.initMocks(this);
    BankUseCase bankUseCase = new BankUseCase();
    bankUseCase.amount = 1000;
    bankUseCase.id = "199";

    ResponseEntity<Object> response = transactionController.deposit(bankUseCase);

    Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode());
    Assertions.assertEquals("Deposit Transaction 👍🏻", response.getBody());
  }

  @Test
  public void create_withdrawal() {
    MockitoAnnotations.initMocks(this);
    BankUseCase bankUseCase = new BankUseCase();
    bankUseCase.amount = 1000;
    bankUseCase.id = "199";

    ResponseEntity<Object> response = transactionController.withdraw(bankUseCase);

    Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode());
    Assertions.assertEquals("Withdraw Transaction 👍🏻", response.getBody());
  }

  @Test
  public void print_account_statement() {
    MockitoAnnotations.initMocks(this);
    Transactions transactions = new Transactions();
    when(inMemoryTransactionRepository.findAll(new Id("199"))).thenReturn(transactions);
    IdUseCase idUseCase = new IdUseCase();
    idUseCase.id = "199";
    when(formatter.print(transactions.createStatement())).thenReturn("printed statement");

    ResponseEntity<Object> response = transactionController.printStatement(idUseCase);

    Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    Assertions.assertEquals("printed statement", response.getBody());
  }

  @Test
  public void throw_an_error_when_service_is_unavailable_in_print_account_statement() {
    MockitoAnnotations.initMocks(this);
    when(inMemoryTransactionRepository.findAll(new Id("199"))).thenReturn(null);
    IdUseCase idUseCase = new IdUseCase();
    idUseCase.id = "199";

    ResponseEntity<Object> response = transactionController.printStatement(idUseCase);

    Assertions.assertEquals(HttpStatus.SERVICE_UNAVAILABLE, response.getStatusCode());
    Assertions.assertEquals("Service Unavailable", response.getBody());
  }

}
